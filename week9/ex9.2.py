#!/usr/bin/env python2
#

import json
from myBagOfWords import myBagOfWords
import numpy as np
from scipy import sparse


data_folder = "data"


def get_complete_filename(no):
    # Accepts the number of file for which build the complete filename.
    if no < 10:
        no = str(no)
        no = "0" + no
    return data_folder + "/reuters-0" + str(no) + ".json"


def clean_json(json_obj):
    # For each dictionary in the json_obj list, returns only the ones with at
    # least the title or a topic.
    return [d for d in json_obj if 'body' in d and 'topics' in d]


def load_and_clean_json_file(filename):
    with open(filename) as fo:
        json_loaded = json.load(fo)
        json_cleaned = clean_json(json_loaded)
        return json_cleaned


def create_bag_of_words(articles):
    """Create a bag of words based on the body of the articles.
    Using a list takes around 7 minutes, a "special" bag of words takes
    instead 1.2 seconds.
    """

    bag = myBagOfWords()
    for d in articles:
        for word in d['body'].lower().split():
            bag.add(word)

    return bag


def encode_articles_bodies_with_bow(articles, bag_of_words):
    """Using the provided bag-of-words, encode the body of each article."""

    # frequency_matrix = [[0] * len(bag_of_words)] * len(articles)
    frequency_matrix = sparse.lil_matrix(
        (len(articles), len(bag_of_words)), dtype=np.uint8)

    for article_index, dict_article in enumerate(articles):
        for word in dict_article["body"].lower().split():
            frequency_matrix[article_index, bag_of_words.index(word)] += 1

    return frequency_matrix


if __name__ == "__main__":
    articles = []
    for i in xrange(22):
        # By concatenating multiple functions like the following we can avoid
        # saving intermediate results.
        articles.extend(load_and_clean_json_file(get_complete_filename(i)))

    articles = articles[:100]
    bow = create_bag_of_words(articles)

    articles_encoded = encode_articles_bodies_with_bow(articles, bow)

    perm1 = np.random.permutation(len(bow))
    perm2 = np.random.permutation(len(bow))

    shuffled1 = []
    for article in articles_encoded:
        # Each article here is a matrix with shape (1, len_bow), so we need to
        # select the first (0) row in order to permute the columns with the
        # permutation set. Furthermore, even nonzero() returns a matrix with
        # shape (1, len_bow), so we have to select the second array (the first
        # is full of zero values) and pick up the first (index = 0) element.
        first_nnz = article[0, perm1].nonzero()[1][0]
        shuffled1.append(first_nnz)

    shuffled2 = []
    for article in articles_encoded:
        # Each article here is a matrix with shape (1, len_bow), so we need to
        # select the first (0) row to permute the columns. Furthermore, even
        # nonzero returns a matrix with shape (1, len_bow)
        first_nnz = article[0, perm2].nonzero()[1][0]  # [0][0]
        shuffled2.append(first_nnz)

    print len(shuffled1), len(shuffled2)
