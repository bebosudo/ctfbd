#!/usr/bin/env python2
#

import json
from myBagOfWords import myBagOfWords
import scipy.sparse as sparse
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics

# DEBUG = False
DEBUG = True

# The number of estimators is the number of trees in the forest.
n_estimators = 50
percentage_training = 0.8
number_of_buckets = 1000

data_folder = "data"


def get_complete_filename(no):
    # Accepts the number of file for which build the complete filename.
    if no < 10:
        no = str(no)
        no = "0" + no
    return data_folder + "/reuters-0" + str(no) + ".json"


def clean_json(json_obj):
    # For each dictionary in the json_obj list, returns only the ones with at
    # least the title or a topic.
    return [d for d in json_obj if 'body' in d and 'topics' in d]


def load_and_clean_json_file(filename):
    with open(filename) as fo:
        json_loaded = json.load(fo)
        json_cleaned = clean_json(json_loaded)
        return json_cleaned


def create_bag_of_words(articles):
    """Create a bag of words based on the body of the articles.
    Using a list takes around 7 minutes, a "special" bag of words takes
    instead 1.2 seconds.
    """

    bag = myBagOfWords()
    for d in articles:
        for word in d['body'].lower().split():
            bag.add(word)

    return bag


def create_frequency_matrix(list_of_articles, bag_of_words):
    """Given the list containing the dictionaries and the bag of words
    OrderedSet, returns a matrix of occurrencies/frequency of each word
    inside the articles.
    """

    # Using ListsOfLists sparse matrix we create the frequency matrix in approx
    # 30 sec, instead of 7 mins.
    frequency_matrix = sparse.lil_matrix(
        (len(list_of_articles), len(bag_of_words)), dtype=np.uint8)

    for article_index, article in enumerate(list_of_articles):
        if DEBUG and article_index % 1000 == 0:
            print article_index

        # For each word in the body of each article, increment the value in the
        # sparse matrix.
        for word in article['body'].lower().split():
            # Since we previously built the bag of word in the same way,
            # there's no need to check if the word is inside the bag of words.
            frequency_matrix[article_index, bag_of_words.index(word)] += 1

    return frequency_matrix


def create_matrix_n_buckets(list_of_articles, bag_of_words, no_buckets):
    """We need to create a frequency matrix using n buckets, instead of using
    directly the bag of words "insertion time". We use the insertion time and
    the modulo operation (bucket[insertion_time%n]) in order to get the bucket
    index where to send the word.
    """

    frequency_matrix = sparse.lil_matrix((len(list_of_articles), no_buckets),
                                         dtype=np.uint8)

    for article_index, article in enumerate(list_of_articles):
        if DEBUG and article_index % 1000 == 0:
            print 1. * article_index / len(list_of_articles), "%"

        for word in article['body'].lower().split():
            frequency_matrix[article_index,
                             bag_of_words.index(word) % no_buckets] += 1

    return frequency_matrix


def create_target_list(list_of_articles):
    """Here we build the booleans target list, used to train and then test
    the classifier on which articles are the "correct" ones.
    """

    target_list = []

    for article in list_of_articles:
        if "earn" in article["topics"]:
            target_list.append(True)
        else:
            target_list.append(False)

    return target_list


if __name__ == "__main__":
    articles = []
    for i in xrange(22):
        # By concatenating multiple functions like the following we can avoid
        # saving intermediate results.
        articles.extend(load_and_clean_json_file(get_complete_filename(i)))

    import random
    random.shuffle(articles)

    bag_of_words = create_bag_of_words(articles)

    freq_mat = create_frequency_matrix(articles, bag_of_words)
    target = create_target_list(articles)
    matrix_1000_buckets = create_matrix_n_buckets(articles,
                                                  bag_of_words,
                                                  number_of_buckets)

    separator = int(percentage_training * freq_mat.shape[0])
    training_data, test_data = freq_mat[:separator, :], freq_mat[separator:, :]
    training_target, test_target = target[:separator], target[separator:]

    training_data_1000 = matrix_1000_buckets[:separator, :]
    test_data_1000 = matrix_1000_buckets[separator:, :]

    clf = RandomForestClassifier(n_estimators=n_estimators)
    clf = clf.fit(training_data, training_target)
    prediction = clf.predict(test_data)
    score = clf.score(test_data, test_target)

    print "The 'normal' classifier had a performance of:", score
    print "Metrics report of the 'normal' classifier:\n", \
        metrics.classification_report(y_true=test_target, y_pred=prediction)

    clf_1000 = RandomForestClassifier(n_estimators)
    clf_1000 = clf_1000.fit(training_data_1000, training_target)
    prediction_1000 = clf_1000.predict(test_data_1000)
    score_1000 = clf_1000.score(test_data_1000, test_target)

    print "The '1000 buckets' classifier had a performance of:", score_1000
    print "Metrics report of the 'normal' classifier:\n", \
        metrics.classification_report(
            y_true=test_target, y_pred=prediction_1000)
