#!/usr/bin/env python2


class myBagOfWords(object):
    """This class behaves like a list, but keeps the uniqueness of elements of
       sets.

       Only adding and searching operations are implemented at the moment.
       The deletion operation could be implemented by using another dictionary
       to store insertion order and word in the reverse order compared to the
       actual storage dictionary, id est 'index_order: word'.
       Internally, this class is implemented as a dictionary (for lookup speed)
       storing the word chosen as key and the order of insertion number as
       value.
       Inspired by the OrderedSet implementation:
        http://code.activestate.com/recipes/576694-orderedset/
       """

    def __init__(self, iterable=None):
        self.storage = {}
        self.dimension = 0

        # If an initial iterable is passed to the constructor, iterate over it
        # and add all the element at the object.
        if iterable is not None:
            for elem in iterable:
                self.add(elem)

    def add(self, key):
        if key not in self.storage:
            self.storage[key] = self.dimension
            self.dimension += 1

    def index(self, key):
        if key not in self.storage:
            raise ValueError("Element {} not present inside {} object.".format(
                key, self.__class__.__name__))
        return self.storage[key]

    def __repr__(self):
        if len(self) == 0:
            return '%s()' % (self.__class__.__name__,)
        return '%s(%r)' % (self.__class__.__name__, self.storage.keys)

    def __len__(self):
        return self.dimension


if __name__ == "__main__":
    first = myBagOfWords([2, 4, "asdf"])
    second = myBagOfWords()
    second.add("elem")

    print first
    print second
