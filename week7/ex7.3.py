#!/usr/bin/env python2
#

from mrjob.job import MRJob
from mrjob.step import MRStep

# i = 0


class MRTrianglesCount(MRJob):
    i = 0

    def steps(self):
        return [
            MRStep(mapper=self.mapper,
                   #    combiner=self.combiner,
                   reducer=self.reducer),
            MRStep(reducer=self.sum_up)
        ]

    def mapper(self, _, input_line):
        self.i = self.i + 1
        if self.i % 10000 == 0:
            print self.i

        node1, node2 = map(int, input_line.split())
        yield node1, 1
        yield node2, 1

    # def combine(self, node, values):
    #     print "sono qui"
    #     # print no_other_nodes
    #     yield node, no_other_nodes  # * (no_other_nodes - 1) / 2)

    def reducer(self, node, values):
        no_other_nodes = sum(values)
        yield None, (no_other_nodes * (no_other_nodes + 1)) / 2

    def sum_up(self, _, generator_number_of_triangles):
        ret = sum(generator_number_of_triangles) / 3
        print ret
        yield None, ret

    # def count_triangles(self, _, no_triangles_per_node):
    #     print "sono qua"

    # def combiner(self, node_name, list_of_triangles_found):
    #     yield None, sum(list_of_triangles_found) / 3


if __name__ == "__main__":
    job = MRTrianglesCount(args=['roadNet-CA/roadNet-CA.mtx'])  # , '-q'])
    # job = MRTrianglesCount(args=['test_ex3.txt'])  # , '-q'])
    job.execute()
