#!/usr/bin/env python2
#

from mrjob.job import MRJob


class MRWordFrequencyCount(MRJob):

    def mapper(self, _, line):
        node1, node2 = map(int, line.split())
        yield node1, node2

    def reducer(self, key, values):
        yield key, len(values)


if __name__ == "__main__":
    MRWordFrequencyCount.run()
