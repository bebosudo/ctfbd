#!/usr/bin/env python2

from mrjob.job import MRJob
from mrjob.step import MRStep
from mrjob.protocol import JSONValueProtocol


class MRTriangleCount(MRJob):

    # Used to only output the value and not the key when printing
    OUTPUT_PROTOCOL = JSONValueProtocol

    # 3 steps job
    def steps(self):
        return [
            MRStep(mapper=self.mapper,
                   reducer=self.reducer),
            MRStep(reducer=self.check_for_triangles),
            MRStep(reducer=self.count_triangles)
        ]

    def mapper(self, _, line):
        # Get node pair
        node1, node2 = map(int, line.split())
        # Yield node pair in both ways to get a list of neighbors for node1,
        # and then node2
        yield node1, node2
        yield node2, node1

    def reducer(self, node, neighbors):
        # Capture the list of neights for the node (as it is a generator
        # object)
        neighbors2 = list(neighbors)
        # For each node in the neighbors list, yield node pair in both ways
        # with the list of neighbors
        print "node:", node
        for some_node in neighbors2:

            yield (node, some_node), neighbors2
            yield (some_node, node), neighbors2

    def check_for_triangles(self, node_pair, neighbors):
        # Get node pair from the key
        node1, node2 = node_pair
        # Get the neighbors for the first node
        edge1_neighbors = set(neighbors.next())
        # Get the neighbors for the second node
        edge2_neighbors = set(neighbors.next())
        # Get all common neighbors in the intersection of each node neighbors :
        # they are the ones that form a triangle with the node pair
        common_nodes = [node for node in edge1_neighbors.intersection(
            edge2_neighbors) if node != node1 and node != node2]

        # Yield the length of the common neighbors, which is equal to the
        # number of triangles for this node pair
        yield None, len(common_nodes)

    def count_triangles(self, _, counts):
        # We now have all the count of triangles in a list, we need to sum it and then divide by :
        # 2*3 because we count twice each triangle (due to the very first mapper), and also three times each triangle
        # due to each point in a triangle
        yield None, sum(counts) / 6

if __name__ == '__main__':
    # -q for quiet : doesn't print the steps
    # job = MRTriangleCount(args=['roadNet-CA/roadNet-CA.mtx', '-q'])
    job = MRTriangleCount(args=['test_ex3.txt', '-q'])
    print "How many triangles in graph ? :"
    job.execute()
