#!/usr/bin/env python2
#

import string
from mrjob.job import MRJob

punctuation_replacer = string.maketrans(
    string.punctuation, " " * len(string.punctuation))


class MRWordFrequencyCount(MRJob):

    def mapper(self, _, line):
        for word in line.translate(punctuation_replacer).lower().split():
            yield word, 1

    def reducer(self, key, values):
        yield key, sum(values)

if __name__ == "__main__":
    MRWordFrequencyCount.run()
