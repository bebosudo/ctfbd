from mrjob.job import MRJob
from mrjob.step import MRStep
from mrjob.protocol import JSONValueProtocol


class MREulerPathChecker(MRJob):

    # Used to only output the value and not the key when printing
    OUTPUT_PROTOCOL = JSONValueProtocol

    # Define the steps for our job
    def steps(self):
        return [
            MRStep(mapper=self.mapper_node_init,
                   reducer=self.reducer_is_even),
            MRStep(reducer=self.reduce_check_for_euler_path)
        ]

    def mapper_node_init(self, _, line):
        # Get node pair on line
        node1, node2 = map(int, line.split())
        # Yield a tuple consisting of the node and the value 1 for each node
        yield (node1, 1)
        yield (node2, 1)

    def reducer_is_even(self, _, counts):
        # For each key, check if the counts is even, and yield the boolean
        # value onto a unique key (None)
        yield None, sum(counts) % 2 == 0

    def reduce_check_for_euler_path(self, _, truths):
        # We only have one key left now (None), lets check the list of booleans
        has_euler_path = True
        # For each truth value, if one is False, there can't be a Euler path
        # (as the graph is complete)
        for truth in truths:
            if not truth:
                has_euler_path = False
                break
        # Yield final value
        yield None, has_euler_path


def run_mr_on_graph(name, file_name):
    # -q for quiet : doesn't print the steps
    job = MREulerPathChecker(args=[file_name, '-q'])
    print "Has %s got an Euler path ? :" % name
    job.execute()

if __name__ == '__main__':
    run_mr_on_graph('Graph1', 'data/eulerGraph1.txt')
    run_mr_on_graph('Graph2', 'data/eulerGraph2.txt')
    run_mr_on_graph('Graph3', 'data/eulerGraph3.txt')
    run_mr_on_graph('Graph4', 'data/eulerGraph4.txt')
    run_mr_on_graph('Graph5', 'data/eulerGraph5.txt')
