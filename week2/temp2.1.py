#!/usr/bin/env python2

def to_matrix(filename):
    with open(filename) as f:
        return [line.split() for line in f]

def from_matrix(matrix, filename):
    with open(filename, 'w') as f:
        for line in matrix :
            f.write(" ".join(line)+"\n")

from_matrix(to_matrix('2.1.txt'),'matrixout.txt')
