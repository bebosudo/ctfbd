#!/usr/bin/env python3

def bitstrings(n):
  return [genstring (i, n) for i in range(2**n)]

def genstring(index, size):
  return [index>>i&1 for i in range(size)][::-1]

print(", ".join(map(str, bitstrings(3))))
