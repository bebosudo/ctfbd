#!/usr/bin/env python3
#
#

import json, re
# from pprint import pprint

with open('pizza-train.json') as data_file:
    request_text_list = [pizza_request["request_text"].lower()
                        for pizza_request in json.load(data_file)]

distinct_words = []
request_text_list_of_lists_no_punct = []
for pizza_text in request_text_list:
    # with the following regex I can select words with no numbers in it.
    # pizza_text_no_punct = re.findall(r'\b([^\d\W]+)\b', pizza_text)

    # with this one I select words such as: house4pi_z_z_a
    pizza_text_no_punct = re.findall(r'\w+', pizza_text)
    for word in pizza_text_no_punct:
        if word not in distinct_words:
            distinct_words.append(word)

    # create a new list of request_text without punctuation
    request_text_list_of_lists_no_punct.append(pizza_text_no_punct)


final_bag_of_words = []
for request_text in request_text_list_of_lists_no_punct:
    partial_bag_of_words = []
    for word in distinct_words:
        partial_bag_of_words.append(request_text.count(word))
    final_bag_of_words.append(partial_bag_of_words)

print(str(len(final_bag_of_words))+"x"+str(len(final_bag_of_words[0])))
# print(final_bag_of_words[:5])
