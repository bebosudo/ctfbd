#!/usr/bin/env python3

import json
import string
import numpy as np

def analyse(words, bag):
  return [[entry.count(word) for word in bag] for entry in words]

def cleanup(entry):
  return list(map(lambda x:x.replace("\n", "  ").strip(string.punctuation).lower(), entry))


def load(file):
  with open(file) as f:
    data = json.loads(f.read())
    words = []
    entryWords = []
    outcomes = []
    for entry in data:
      entries = cleanup(entry["request_text"].split())
      outcomes.append(entry["requester_received_pizza"])
      entryWords.append(entries)
      words.extend(entries)

    bag = set(words)

    np.save("data", np.insert(analyse(entryWords, bag), 0, outcomes, axis =1) )

load("pizza-train.json")
