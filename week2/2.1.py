#!/usr/bin/env python3
#
# Exercise 2.1

def read_matrix(in_fname):
    # Given in_fname a correct file path to open a file whose content is a
    # matrix, this function will return a list of list, namely a list containing
    # the rows of the matrix.

    # first version, return a list of lists of strings: [['2', '4'], [1, 13]]
    # return [line.rstrip().split(" ") for line in open(in_fname, 'r')]

    # this version returns a list of lists of integers:
    # return [[int(x) for x in line.rstrip().split(" ")] for line in open(in_fname, 'r')]
    return [[int(x) for x in line.rstrip().split(" ")] for line in open(in_fname, 'r')]


def save_list_of_lists(list_of_lists, out_fname):
    # Given a list of lists containing values, this function will save in the
    # file called out_fname a matrix made of the values above.

    # Should I do a test on the correctness of the input list?

    with open(out_fname, 'w') as out_fo:
        for list_ in list_of_lists:
            # if I have a list of lists of strings I should use:
            # out_fo.write(' '.join(list_)+'\n')
            out_fo.write(' '.join((str(x) for x in list_))+'\n')



# uncomment the triple quotes around the main function to test the script.

def __main__():
    file_in = "1.txt"
    file_out = file_in+".out"

    # print the starting matrix
    print("Here's the initial matrix:")
    for line in open(file_in):
        print(line, end='')

    # print the list of lists
    print(file_in.join(("\nThis is a list of lists containing the elements of "
                        "the matrix inside file ", ":")))
    print(read_matrix(file_in))

    # save the matrix to file and print a confirm message
    save_list_of_lists(read_matrix(file_in), file_out)
    print(file_out.join(("\nMatrix saved to file ", ".")))


__main__()
