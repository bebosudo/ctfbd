#!/usr/bin/env python2

def list_creator(number):
    # Control on the input.
    try:
        number = int(number)
    except ValueError:
        print("Insert a number as argument to the script.")
        raise SystemExit
    if number < 0:
        print("The input number has to be a positive integer.")
        raise SystemExit

    full_list = []
    for element in range(2**number):
        partial_list = []
        n = element
        while (n > 0):
            # Insert at the head of the list the modulus (= the rest
            # of the division) of n, until n reach zero.
            partial_list.insert(0, n % 2)
            # Unambiguous floor division (works on every python >2.2)
            n = n // 2
        if len(partial_list) != number:
            # Fill the head of the list with zeroes, if they are missing.
            for _ in range(number-len(partial_list)):
                partial_list.insert(0, 0)
            # (it could have been written shorter as: )
            # partial_list = [0]*(number-len(partial_list)) + partial_list

        full_list.append(partial_list)
    return full_list

if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print("Please run this script with a single positive number as argument.")
        raise SystemExit

    for comb in list_creator(sys.argv[1]):
        print comb
