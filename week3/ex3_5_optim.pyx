#!/usr/bin/env python3
#

def sum_reciprocals(int end_number):
    cdef double result = 0
    cdef int i = 1
    while i < end_number:
        result += 1.0/i**2
        i += 1
    return result

# for _ in range(500):
#     sum_reciprocals(10000)
