#!/usr/bin/env python3
#

def sum_reciprocals(end_number):
    result = 0
    for i in range(1, end_number):
        result += 1.0/i**2
    return result

for _ in range(500):
    sum_reciprocals(10000)
