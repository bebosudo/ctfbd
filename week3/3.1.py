#!/usr/bin/env python2
import numpy as np

def fetch_matrix_from_txt(filename):
	with open(filename) as fobj:
		# The double list comprehension could be replaced by the map
		# builtin, but it ' s not so readable and could be even slower.
		return [[int(num) for num in line.rstrip().split(",")] for line in fobj]

def extract_coefficient_and_constant_terms(list_of_lists_matrix):
	matrix = np.array(list_of_lists_matrix)
	# Using array slicing we can divide the matrix into the
	# coefficients matrix and constants vector.
	# http://docs.scipy.org/doc/numpy/reference/arrays.indexing.html
	coeffs, consts = matrix[:, :-1], matrix[:, -1]
	return coeffs, consts

if __name__ == "__main__":
	# Merge the two functions in a single operation
	a, b = extract_coefficient_and_constant_terms(fetch_matrix_from_txt("3.1.txt"))
	print "This is the matrix of coefficients:\n", a,\
	"\n\nand this is the constant terms vector:\n", b

	x = np.linalg.solve(a, b)
	print "\nThese are the solutions of the linear system:\n", x

	print "\nCheck correctness of result: (Ax =? b) ==", np.allclose(np.dot(a, x), b)
