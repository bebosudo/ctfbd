#!/usr/bin/env python2

import scipy
from scipy import optimize
import numpy as np
import matplotlib.pyplot as plt


def read_data(file):
	x,y = np.loadtxt(file, unpack = True)
	z = scipy.polyfit(x, y, 3)
	p = np.poly1d(z) # convert to proper polynomium function
	xs = np.linspace(-20, 20,1000) # generate plotting points
	ys = p(xs)
	plt.plot(xs, ys)
	plt.plot(x, y, 'ro')
	plt.show()
	return p

def find_roots(eq):
	sol = scipy.optimize.root(eq, 0, method = 'df-sane')
	print sol.x

eq = read_data('3.2.txt')
find_roots(eq)
