#!/usr/bin/env python3
#
# Todo 1
#
# Using the movie-lens 1M data and pandas.read_table read in all three files (users, ratings, movies) into pandas DataFrames.
# I recommend giving columns names directly to read_table for each case.
## HINT: you will need to open the files in a text editor to see what the separator character. Also, the columns for each file correspond to the following:
#     users.dat: user id, gender, age, occupation code, zip
#     ratings.dat: user id, movie id, rating, timestamp
#     movies.dat: movie id, title, genre
#
# Use the data combining tools discussed above to combine these three objects into a single object named movie_data

import pandas as pd

movies_df = pd.read_csv("ml-1m/movies.dat", sep="::", engine="python",
                    names=["movieID", "title", "genre"])
ratings_df = pd.read_csv("ml-1m/ratings.dat", sep="::", engine="python",
                    names=["userID", "movieID", "rating", "timestamp"])
users_df = pd.read_csv("ml-1m/users.dat", sep="::", engine="python",
                    names=["userID", "gender", "age", "occupation_code", "zip"])

movie_data = pd.merge( pd.merge(users_df, ratings_df), movies_df)



# Todo 2
#
# Use the movie_data object from the previous exercise and compute the following things:
#
#   -  The 5 movies with the most number of ratings
#
#   -  A new object called active_titles that is made up of movies each having at least 250 ratings
#
#   -  For the subset of movies in the active_titles list compute the following:
#             The 3 movies with the highest average rating for females. Do the same for males.
#             The 10 movies men liked much more than women and the 10 movies women liked more than men
#                   (use the difference in average ratings and sort ascending and descending).
#             The 5 movies that had the highest standard deviation in rating.
#
# HINT: For some of these operations it might be helpful to compute a pivot table using the pivot_table method of a DataFrame.
#
# NOTE: You might also want to do some analysis on the movie genre. To do this you would have to be
#       comfortable with using pandas vectorized string methods and probably some regular expressions.
#       Those topics are a bit bigger than what we can cover here, but if you are looking for more
#       practice, they are a good place to start.


print(movie_data.groupby(by="movieID", sort=False).size().sort_values().tail(5).reset_index(name='size').loc[:, 'movieID'])
raise SystemExit

m = movie_data.groupby(by="movieID", sort=False).size().reset_index(name='size')
active_titles = m[m['size'] >= 250]

subset = pd.merge( pd.merge(active_titles, ratings_df, on='movieID'), users_df, on='userID')

# The following lines will produce dataframes with two columns, the movieID and
# the average of the ratings for each movie.
female_movie_ratings = subset[subset["gender"] == "F"][["movieID", "rating"]].groupby("movieID").mean().rename(columns={'rating': 'rating_f'}).reset_index()
male_movie_ratings = subset[subset["gender"] == "M"][["movieID", "rating"]].groupby("movieID").mean().rename(columns={'rating': 'rating_m'}).reset_index()
print(female_movie_ratings.columns.values.tolist(), '\n\n')

female_3top = female_movie_ratings.nlargest(3, columns='rating_f')
male_3top = male_movie_ratings.nlargest(3, columns='rating_m')

print(female_3top, "\n\n", male_3top)

# these lines will make the pc freeze
comp = pd.merge(female_movie_ratings, male_movie_ratings, on="movieID")
comp["comparison"] = pd.Series(comp["rating_f"]-comp["rating_m"])

ten_more_female = comp.nlargest(10, "comparison")
ten_male = comp.nsmallest(10, "comparison")


print(ten_female, "\n\n", ten_male)
