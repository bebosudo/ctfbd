#!/usr/bin/env python3
#

from scipy import interpolate, optimize
import numpy as np
import matplotlib.pyplot as plt

def read_points(filename):
    x_list, f_x_list = [], []
    for line in open(filename):
        x, f_x = line.rstrip().split(" ")
        x_list.append(int(x))
        f_x_list.append(float(f_x))
    return x_list, f_x_list
    # return [[(int(couple[0]), float(couple[1])) for couple in line.rstrip().split(" ")] ... ]

x, y = read_points("3.2.txt")
f = interpolate.interp1d(x, y)

print(f)

xnew = np.arange(x[0], x[-1], 0.1)
ynew = f(xnew)

print("The root of the polynomial function is at: ")
print(optimize.brentq(f, x[0], x[-1]))

plt.plot(x, y, 'o', xnew, ynew, '-')
user_choice = input("Digit s to show the plot of the function [s]: ")
if user_choice == 's':
    plt.show()
