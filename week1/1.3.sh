#!/usr/bin/env bash
tr '[:upper:]' '[:lower:]' < dict |sort |uniq > dict2
tr '[:punct:]' ' ' < shakespeare.txt |tr -s '[:space:]' '\n' |tr '[:upper:]' '[:lower:]' |sort -u |comm -13 dict2 - |wc -l
