import ast
from operator import add

# N regulates the number of most visited networks we want to extract.
N = 10
data_filename = "wifi.data"


fo = sc.textFile(data_filename)

# Using ast.literal_eval compared to the classic eval helps to mitigate some security risks:
# - about the risks of using a plain eval: http://nedbatchelder.com/blog/201206/eval_really_is_dangerous.html
# - about the "benefits" of ast.literal_eval compared to eval(): http://stackoverflow.com/a/15197726
list_of_bssids = fo.map(lambda line: ast.literal_eval(line.rstrip()))



#######################################################################################
# part 1

# Group the networks using (ssid, bssid). Ssid isn't unique, but bssid is, so the whole key is unique:
# this is called a `superkey` in the DB theory.
# We have chosen to select even the ssid in order to be able to display it at the end, without needing
# to join the bssid again with the initial list.
bssids_and_logs = list_of_bssids.groupBy(lambda wifi_dict: (wifi_dict['ssid'], wifi_dict['bssid']))

# We map each superkey (ssid, bssid) to the number of times it appear in the data.
bssids_and_no_logs = bssids_and_logs.map(lambda bssid_with_logs: (bssid_with_logs[0], len(bssid_with_logs[1])))

# The -x[1] in the function orders the networks descending depending on the second (1) field, id est
# the number of times it appeared.
N_most_visited = bssids_and_no_logs.takeOrdered(N, lambda x: -x[1])

print "The {} most seen different networks:".format(N)
for (netw_ssid, netw_bssid), n_times_seen in N_most_visited:
    print u"Network {} (with BSSID: {}) has been seen {} times.".format(
                                            netw_ssid.join("' '".split()),
                                            netw_bssid,
                                            n_times_seen)

print "%"*40, "\n"

#############################################################################
# part 2

ssids = list_of_bssids.map(lambda wifi_dict: (wifi_dict['ssid'], 1))

most_frequent_ssids = ssids.reduceByKey(add).takeOrdered(N, lambda x: -x[1])

print "The {} most seen networks' names:".format(N)
for ssid, frequency in most_frequent_ssids:
    print u"Network with SSID '{}' has been seen {} times.".format(ssid, frequency)


print "%"*40, "\n"

#############################################################################
# part 3

ssids = list_of_bssids.map(lambda wifi_dict: (wifi_dict['ssid'], len(wifi_dict['ssid']))).distinct()

most_long_ssids = ssids.takeOrdered(N, lambda x: -x[1])

print "The {} longest networks' names:".format(N)
for ssid, length in most_long_ssids:
    print u"The SSID of network '{}' is {} chars long.".format(ssid, length)
