#!/usr/bin/env python2
#
# Use this script inside a Apache Spark shell, or setup a connection to the Spark server on 
# the 'sc' variable.


# Instead of writing a specific lambda function to reduce elements by addition, we can simply
# use operator.add() function, which is the one used internally by Python to resolve addition.
from operator import add

# Inside the spark environement provided there are some sources, and among them there is even
# this Shakespeare's sonnet.
fo = sc.textFile("data/cs100/lab1/shakespeare.txt")

# This flatMap returns all the input elements as a single list of words.
dict_of_words = fo.flatMap(lambda line: line.lower().split()).map(lambda word: (word, 1))
dict_of_words.reduceByKey(add)
dict_of_words.collect()
