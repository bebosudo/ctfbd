#!/usr/bin/env python2
#


for graph_no in xrange(1, 6):
    fo = sc.textFile("eulerGraphs/eulerGraph{}.txt".format(graph_no))
    nodes_combinations = fo.flatMap(lambda line: (map(int, line.split()),
                                                  map(int, line.split()[::-1])))
    no_edges_per_node = nodes_combinations.countByKey()
    for node in no_edges_per_node:
        no_edges_per_node[node] = no_edges_per_node[node] % 2 != 0

    print "Graph {} {} an Euler tour.".format(graph_no,
                                              "has"
                                              if not any(
                                                  no_edges_per_node.values())
                                              else "has not"
                                              )
