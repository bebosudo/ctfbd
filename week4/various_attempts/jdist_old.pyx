#!/usr/bin/env python2
#

import numpy as np
cimport numpy as np

ctypedef int DTYPE_t


def jdist(np.ndarray[DTYPE_t, ndim=1] p1, np.ndarray[DTYPE_t, ndim=1] p2):
    """Return the Jaccard distance between two points.

    Input args: two points p1 and p2 encoded as numpy arrays.
    Output: float.

    By definition, the Jaccard index is the size of the intersection divided
    by the size of the union of two sets of elements.
    If the two sets are vectors of positive real numbers, like as in this
    case are points, the Jaccard index can be calculated as the summation of
    the minimum for each index divided by the summation of the maximum for
    each index.

    Example:
    p0 = (x0, x1, x2, x3) = (2, 0, 4, 3) and
    p1 = (y0, y1, y2, y3) = (0, 0, 1, 5).

    In this case, the summation on intersection/minimum is 0+0+1+3 = 4 and
    the summation on union/maximum is 2+0+4+5 = 11, hence the Jaccard index
    is 4/11=0.36, and the Jaccard distance is 1 - 4/11=7/11 = 0.64
    """

    # int summation_union
    # cdef int summation_intersct
    intersect = np.intersect1d(p1, p2)
    # cdef int summation_intersct = len(set(p1 + p2))
    cdef int summation_intersection = intersect.size

    union = np.union1d(p1, p2)
    # cdef int summation_union = len(set(p1.extend(p2)))
    cdef int summation_union = union.size

    dist = 1 - 1. * summation_intersection / summation_union
    # The Jaccard distance is = 1 - (Jaccard index).
    return dist
