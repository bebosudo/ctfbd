#!/usr/bin/env python2
#

# Every matrix in this exercise can be checked with the max() method of
# csr_matrix, and the only values found are zeroes and ones. This helps in the
# calculus of Jaccard distance, since min and max can only be 0s or 1s.


import pickle
import numpy as np
from scipy.sparse import csr_matrix


def load_and_parse_matrix(filename):
    with open(filename) as fo:
        return csr_matrix(pickle.load(fo))


# https://en.wikipedia.org/wiki/Jaccard_index#Generalized_Jaccard_similarity_and_distance
def jdist(p1, p2):
    """Return the Jaccard distance between two points.

    Input args: two points p1 and p2 encoded as numpy arrays.
    Output: float.

    By definition, the Jaccard index is the size of the intersection divided
    by the size of the union of two sets of elements.
    If the two sets are vectors of positive real numbers, like as in this
    case are points, the Jaccard index can be calculated as the summation of
    the minimum for each index divided by the summation of the maximum for
    each index.

    Example:
    p0 = (x0, x1, x2, x3) = (2, 0, 4, 3) and
    p1 = (y0, y1, y2, y3) = (0, 0, 1, 5).

    In this case, the summation on intersection/minimum is 0+0+1+3 = 4 and
    the summation on union/maximum is 2+0+4+5 = 11, hence the Jaccard index
    is 4/11=0.36, and the Jaccard distance is 1 - 4/11=7/11 = 0.64
    """

    intersct = np.intersect1d(p1.indices, p2.indices)
    summation_intersct = intersct.size

    union = np.union1d(p1.indices, p2.indices)
    summation_union = union.size

    # The Jaccard distance is = 1 - (Jaccard index).
    jaccard_distance = 1 - 1.*summation_intersct/summation_union
    return jaccard_distance


# https://en.wikipedia.org/wiki/DBSCAN#Algorithm
#
def region_query(dataset, point, eps):
    """Given the whole dataset and a point, return the eps-neighbors point.

    The returning list contains only the indices of the elements in the
    dataset.
    """

    # This is the long way to write the line below, that should be even faster.
    # neighborhood = []
    # for pt_num in xrange(dataset.get_shape()[0]:
    #     if jdist(pt_num, point) <= eps:
    #         neighborhood.append(pt_num)
    return [pt_num for pt_num in xrange(dataset.get_shape()[0])
            if jdist(dataset[pt_num], point) <= eps]


def expand_cluster(point_num, neighborhood, cluster, min_pt_cluster, eps,
                   list_of_clusters, visited_points, dataset):
    """
    Compared to the wikipedia version, there's a list_of_clusters parameter
    more (in order to search out if the element is inside any cluster yet),
    the visited_points list and the whole dataset (to retrieve points).

    The point_num point can be calculated using it as an offset on the dataset.

    The neighborhood argument is a list of indices 'eps' away from point_num,
    which can be retrieved by looking up in the whole dataset.
    """

    cluster.append(point_num)
    # visited_points = [False]*len(neighborhood)

    # print visited_points

    for pt_num in xrange(len(neighborhood)):
        # print pt_num
        if visited_points[pt_num]:
            visited_points[pt_num] = True

            # print "dataset:\n", dataset, "\n\n"
            # print "dataset[neighborhood[pt_num]]:\n", dataset[neighborhood[pt_num]]
            # print "neighborhood:\n", neighborhood

            neighborhood_new = region_query(dataset,
                                            dataset[neighborhood[pt_num]], eps)
            if len(neighborhood_new) >= min_pt_cluster:
                neighborhood = list(set(neighborhood + neighborhood_new))

        # Check if pt_num is not part of another cluster.
        for c in list_of_clusters:
            if pt_num in c:
                cluster.append(pt_num)
                return


def DBSCAN(dataset, eps, min_pt_cluster):
    """
    The dataset argument is a matrix, or namely an array of points, or namely
    an array of arrays.
    """
    cluster_num = 0
    list_of_clusters = []

    # Keep track of visited pts throw an array as large as the number of pts.
    visited_points = [False]*dataset.get_shape()[0]
    noise_points = []

    for pt_num in xrange(dataset.get_shape()[0]):
        if visited_points[pt_num]:
            continue

        visited_points[pt_num] = True
        neighborhood = region_query(dataset, dataset[pt_num], eps)

        if len(neighborhood) < min_pt_cluster:
            noise_points.append(pt_num)
        else:
            cluster_num += 1
            new_cluster = []
            expand_cluster(point_num=pt_num, neighborhood=neighborhood,
                           cluster=new_cluster, min_pt_cluster=min_pt_cluster,
                           eps=eps,
                           list_of_clusters=list_of_clusters,
                           visited_points=visited_points, dataset=dataset)
            list_of_clusters.append(new_cluster)

    print cluster_num, list_of_clusters


if __name__ == '__main__':
    dataset = load_and_parse_matrix("test_files/data_10points_10dims.dat")
    # point1, point2 = dataset[1], dataset[6]
    dataset2 = load_and_parse_matrix("test_files/data_100points_100dims.dat")

    DBSCAN(dataset, 0.4, 2)
    DBSCAN(dataset2, 0.3, 2)
