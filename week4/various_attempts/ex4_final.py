#!/usr/bin/env python2

# Import the optimized function.
from jdist3opt import jdist3 as jdist

import cPickle as pickle

# DEBUG = True
DEBUG = False


class DBSCAN(object):
    """Store points indices (compared to the original given dataset) for
    clusters and outliers group in lists inside lists, namely clusters_list and
    outliers_list.
    """

    def __init__(self, input_file_dataset, eps, min_pt_cluster):
        try:
            with open(input_file_dataset) as fo:
                self.dataset = pickle.load(fo)
        except IOError:
            print "Input file {} not found.".format(input_file_dataset)
            raise SystemExit

        self.eps, self.min_pt_cluster = eps, min_pt_cluster

        self.dataset_len = self.dataset.get_shape()[0]

        self.clusters_list = []
        self.outliers_list = []

        self.already_executed = False

    def jaccard_dist(self, pt1, pt2):
        return jdist(self.dataset[pt1].indices, self.dataset[pt2].indices)

    def are_point_close(self, pt1, pt2):
        return self.jaccard_dist(pt1, pt2) <= self.eps

    def is_pt_near_group(self, point, group_of_points):
        """Check if a given point is near one of the point the group given (as
        e.g. a cluster).

        Returns a boolean if the point has a neighbor in the group."""
        for pt in group_of_points:
            if self.are_point_close(point, pt):
                # Here is one of the main differences with the wikipedia alg.
                # If a point close enough to the group has been found,
                # interrupt the search and then merge the groups with that
                # point in common.
                return True
        return False

    def does_pt_belong_to_list(self, point, list_to_check_on):
        """Check if point passed is near one of the point inside the sets in
        the given list.

        Returns the indices of groups to merge with the point."""

        # Indexes of sets to merge.
        return [grp_i for grp_i, grp in enumerate(list_to_check_on)
                if self.is_pt_near_group(point, grp)]

    def merge_groups_given_indices(self, indices, list_of_groups):
        # The 'indices' list is already ordered (look at the function
        # does_pt_belong_to_list).

        if len(indices) > 1:
            # Merge all the sets in the first.
            for i in indices[1:]:
                if DEBUG:
                    print "faccio il merge di", list_of_groups[i], "in", \
                        list_of_groups[0]

                list_of_groups[indices[0]] += list_of_groups[i]

            # Delete the sets starting from the last one to the right until
            # reaching the second to the left.
            for i in indices[:0:-1]:
                del list_of_groups[i]

    def execute(self):

        for pt in xrange(self.dataset_len):
            if DEBUG:
                print "\n", pt

            # Returns the indices of clusters that the point belongs to.
            clusters_indices_to_merge = self.does_pt_belong_to_list(
                pt,
                self.clusters_list)

            if DEBUG:
                print " clusters_indices_to_merge:", clusters_indices_to_merge
                print " found the point in the clusters with these indices", \
                    clusters_indices_to_merge

            # Returns the indices of outliers groups that the point belongs to.
            outliers_indices_to_merge = self.does_pt_belong_to_list(
                pt,
                self.outliers_list)

            if DEBUG:
                print "   outliers_indices_to_merge", outliers_indices_to_merge
                print "   found the point in outliers with these indices", \
                    outliers_indices_to_merge

            if clusters_indices_to_merge:
                # Merge the clusters found: the point will be added later.

                if DEBUG:
                    print "clusters_indices_to_merge", \
                        clusters_indices_to_merge
                    print "self.clusters_list", self.clusters_list

                # Merge all the clusters neighbors into the one with the lower
                # index.
                self.merge_groups_given_indices(clusters_indices_to_merge,
                                                self.clusters_list)

            if outliers_indices_to_merge:
                # Merge all the outliers groups neighbors into the one with the
                # lower index.
                self.merge_groups_given_indices(outliers_indices_to_merge,
                                                self.outliers_list)

            if clusters_indices_to_merge and outliers_indices_to_merge:
                # This means that the point belongs to one (or more) clusters
                # and at the same time to one (or more) outliers groups.

                # Promote the new outliers group to become part of the new
                # cluster just created above (duplicates are not allowed, since
                # all the groups are sets).
                if DEBUG:
                    print "cluster prima del merge", \
                        self.clusters_list[clusters_indices_to_merge[0]]

                self.clusters_list[clusters_indices_to_merge[0]] += \
                    self.outliers_list[outliers_indices_to_merge[0]]

                self.clusters_list[clusters_indices_to_merge[0]].append(pt)

                # Delete the outlier group.
                del self.outliers_list[outliers_indices_to_merge[0]]

                if DEBUG:
                    print "cluster dopo il merge", \
                        self.clusters_list[clusters_indices_to_merge[0]]
                    print "clusters_list", self.clusters_list
                    print "outliers_list", self.outliers_list
                # continue

            elif clusters_indices_to_merge:
                # If the point is a neighbor of only clusters, add it to them.
                self.clusters_list[clusters_indices_to_merge[0]].append(pt)

            elif outliers_indices_to_merge:
                # If the point is a neighbor of only outliers groups, add it to
                # them.
                self.outliers_list[outliers_indices_to_merge[0]].append(pt)

                # If the outliers group just created is big enough to become a
                # cluster, promote it and then remove it from outliers.
                if (len(self.outliers_list[outliers_indices_to_merge[0]]) >=
                        self.min_pt_cluster):
                    self.clusters_list.append(
                        self.outliers_list[outliers_indices_to_merge[0]])

                    if DEBUG:
                        print "Grp {} is large enough to become a " \
                            "cluster".format(self.outliers_list[
                                outliers_indices_to_merge[0]])

                    # And then remove it from the list of outliers.
                    del self.outliers_list[outliers_indices_to_merge[0]]

            else:
                # If the point is not a neighbor of any point in any cluster
                # nor outliers group, add it as a new list in the list of
                # outliers.
                self.outliers_list.append([pt])

                if DEBUG:
                    print "appending pt {} to the outliers".format(pt)

            if DEBUG:
                print "clusters_list", self.clusters_list
                print "outliers_list", self.outliers_list

        self.already_executed = True

    # Pretty printing functions.
    def n_points_in_largest_cluster(self):
        largest = 0
        for cl in self.clusters_list:
            if len(cl) > largest:
                largest = len(cl)
        return largest

    def __str__(self):
        if not self.already_executed:
            return ("Execute the method DBSCAN on the object, in order to see "
                    "various stats.")

        num_points = 0
        for cl in self.clusters_list:
            num_points += len(cl)
        for out in self.outliers_list:
            num_points += len(out)

        # from copy import deepcopy
        # cl = deepcopy(self.clusters_list)
        # ou = deepcopy(self.outliers_list)
        # tot = []
        # for st in cl:
        #     tot += st
        # for st in ou:
        #     tot += st
        # tot.sort()
        # print "\n\n", tot, "\n\n"

        return ("The dataset with length {} has:\n  {} clusters\n+ {} groups "
                "of points not in a cluster (noise)\n= {} total number of "
                "groups.\nThere are {} points at the beginning, and at the end"
                " there are {} points subdivided in clusters and noise\n"
                "There are {} points in the largest cluster found."
                # "\nComplete set:\n{}\nDifference:{}."
                "".format(
                    self.dataset_len,
                    len(self.clusters_list),
                    1,
                    len(self.clusters_list) + 1,
                    self.dataset_len,
                    num_points,
                    self.n_points_in_largest_cluster()
                    # tot,
                    # tot - set(xrange(100))
                )
                )


if __name__ == "__main__":
    eps = {10: 0.4,
           100: 0.3,
           1000: 0.15,
           10000: 0.15,
           100000: 0.15
           }

    dim = 100
    testfile = "test_files/data_{0}points_{0}dims.dat".format(dim)
    d = DBSCAN(testfile, eps[dim], 2)
    d.execute()
    print d


"""
A point can be part of a cluster(or more)
A point can be part of an outlier group(or more)
A point can be part of one(or more) clusters and one(or more) outliers groups
A point can be a new outlier
"""
