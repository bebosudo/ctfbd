#!/usr/bin/env python2

from __future__ import division

# Still have to test this kind of jdist.


def jdist4(seq1, seq2):
    set1, set2 = set(seq1), set(seq2)
    return 1 - len(set1 & set2) / len(set1 | set2)
