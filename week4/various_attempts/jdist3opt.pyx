#!/usr/bin/env python2

# This is the fastest implementation I have been able to produce.

from __future__ import division
import cython
cimport cython

import numpy as np
cimport numpy as np


# from scipy.sparse import csr_matrix

ctypedef int DTYPE_t


def jdist3(np.ndarray[DTYPE_t, ndim=1] vec1, np.ndarray[DTYPE_t, ndim=1] vec2):
    """The function accepts the indices of two points (two numpy.arrays like
    [3, 4, 13, 48]) and calculates the Jaccard distance between them.
    """

    # cdef np.ndarray vec1 = point1.indices
    # cdef np.ndarray vec2 = point2.indices

    cdef int i = 0
    cdef int j = 0

    cdef int intersection = 0

    # To calculate the intersection, the iteration stops when one of the two
    # arrays has no more indexes.
    while i < vec1.shape[0] and j < vec2.shape[0]:
        if vec1[i] == vec2[j]:
            intersection += 1
            i += 1
            j += 1
        else:
            if vec1[i] > vec2[j]:
                j += 1
            else:
                i += 1

    # The union can be thought as in Venn's diagrams, avoiding calculations.
    cdef int union = vec1.shape[0] + vec2.shape[0] - intersection

    # I imported the true division from python3, so there's no more need of the
    # floating cast (maybe it's more efficient).
    return 1 - intersection / union
