""" A DBSCAN implementation follwing the algorithm listed on fraaaanz """


def is_point_near_group(point, group, epsilon):

    for group_point in group:
        if jdist3(point, group_point) < epsilon:
            return True
    return False


def get_near_group_index_list(point, group_list, epsilon):

    return [group_index for group_index, group in enumerate(group_list)
            if is_point_near_group(point, group, epsilon)]


def multipop(list_list, index_list):
    removed_list_content = []

    for index in index_list[::-1]:
        removed_list_content += list_list.remove(index)

    return removed_list_content


def wikiscan(point_list, epsilon, min_cluster_size):
    cluster_list = []
    outlier_group_list = []

    for point in point_list:
        near_cluster_index = None
        near_cluster_index_list = get_near_group_index_list(point=point,
                                                            cluster_list,
                                                            epsilon)

        if near_cluster_index_list:

            near_cluster_index = near_cluster_index_list[0]
            cluster[near_cluster_index].append(point)

            if len(near_cluster_index_list) > 1:

                cluster[
                    near_cluster_index] += multipop(cluster_list, near_cluster_index_list[1:])

        near_outlier_group_index_list = get_near_group_index_list(
            point, outlier_group_list, epsilon)

        if near_outlier_group_index_list:
            if len(near_outlier_group_index_list) >= 1:
                outlier_group_list[near_outlier_group_index_list] += multipop(
                    outlier_group_list, near_outlier_group_index_list[1:])

            if near_cluster_index:
                cluster_list[
                    near_cluster_index] += outlier_group_list.remove(near_outlier_group_index_list[0])
