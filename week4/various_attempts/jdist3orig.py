def jdist3(point1, point2):

    vec1 = point1.indices
    vec2 = point2.indices

    i = 0
    j = 0

    intersection = 0

    while i < len(vec1) and j < len(vec2):
        if vec1[i] == vec2[j]:
            intersection += 1
            i += 1
            j += 1
        else:
            if vec1[i] > vec2[j]:
                j += 1
            else:
                i += 1

    union = len(vec1) + len(vec2) - intersection

    return 1 - float(intersection) / union
