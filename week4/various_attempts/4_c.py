#!/usr/bin/env python2

#
# Every matrix in this exercise can be checked with the max() method of
# csr_matrix, and the only values found are zeroes and ones. This helps in the
# calculus of Jaccard distance, since min and max can only be 0s or 1s.


import cPickle as pickle
# import numpy as np
from scipy.sparse import csr_matrix

from jdist3opt import jdist3 as jdist

# DEBUG = True
DEBUG = False


def load_and_parse_matrix(filename):
    with open(filename) as fo:
        return csr_matrix(pickle.load(fo))


class DBSCAN(object):
    # https://en.wikipedia.org/wiki/Jaccard_index#Generalized_Jaccard_similarity_and_distance
    # https://en.wikipedia.org/wiki/DBSCAN#Algorithm

    # The effective evaluation of the points is done only inside the distance
    # function: in all the other cases the points are managed by their index
    # compared to the dataset below.
    dataset = None
    dataset_length = None
    eps = None
    min_pt_cluster = None

    # The population of these two lists is explained in the docstring of
    # DBSCAN method.
    clusters_list = []
    outliers_list = []

    # Store the eps-neighbors for each point.
    distance_neighbors = {}

    dbscan_executed = False

    def __init__(self, dataset, eps, min_pt_cluster):
        self.dataset = dataset
        self.eps = eps
        self.min_pt_cluster = min_pt_cluster

        # There should be a class wrapper around dataset, in order to
        # generalize the information to retrieve from it, as the length.
        self.dataset_length = dataset.get_shape()[0]

        # Initialize the dict with an empty set of neighbors for each point.
        self.distance_neighbors = dict.fromkeys(
            xrange(self.dataset_length), None)
        for i in self.distance_neighbors:
            self.distance_neighbors[i] = set([])

        self.populate_distance_dict()

        # Keep track of visited pts as an array of bool, large as the number
        # of pts.
        # self.visited_points = [False] * self.dataset_length

    def check_dist(self, pt1, pt2):
        """A simple wrapper around the jdist calculator."""
        point1, point2 = self.dataset[pt1], self.dataset[pt2]
        # print point1.toarray(), "\n----\n", type(point2)
        # raise SystemExit
        # print "\n\n\n****\n", type(point1.indices)
        return jdist(point1.indices, point2.indices) <= self.eps

    def is_neighbor_near_to_point(self, point, neighbor):
        return neighbor in self.distance_neighbors[point]

    def is_pt_near_group(self, pt, group_as_list):
        # print "group_as_list", group_as_list
        for el in group_as_list:
            if self.is_neighbor_near_to_point(pt, el):
                return True
        return False

    def populate_distance_dict(self):
        for pt in xrange(self.dataset_length):
            for neighbor in xrange(self.dataset_length):
                if self.check_dist(pt, neighbor):
                    # Add the effective neighbor to the set of neighbors in the
                    # dictionary.
                    self.distance_neighbors[pt].add(neighbor)

    def merge_point_and_groups(self, pt, list_of_groups):
        """If a point is the link between multiple (2+) groups (e.g.
        list of outliers, or clusters), merge them and the linking point
        and return the new group (as a list)."""

        result = (pt)
        for lst in list_of_groups:
            result.extend(lst)
        # TODO: set() might not be needed and could decrease performances.
        return list(set(result))

    def fit_point_in_groups(self, pt, list_of_groups):
        """Given a point and a list of groups (lists of list of points), check if
        the point is near one (or more) group, and only in that case add it
        to such group(s).

        Returns the new list with the point inside (only if it satisfy the
        distance) and a boolean to sync if the insertion has been done.
        """

        added_pt_to_some_group = False

        indexes_of_groups_to_join = (gi for gi, g in enumerate(
            list_of_groups) if self.is_pt_near_group(pt, g))

        if indexes_of_groups_to_join:
            # Pass only the groups which need to be merged with the point.
            to_join = (list_of_groups[gi] for gi in indexes_of_groups_to_join)

            new_grp = self.merge_point_and_groups(pt, to_join)
            if DEBUG:
                print "Nuovo gruppo creato:", new_grp
            # To remove elements from a list, we need to remove them in the
            # reverse order.
            for grp_i in indexes_of_groups_to_join[::-1]:
                if DEBUG:
                    print "cancello il gruppo con indice", grp_i, "ossia",\
                        list_of_groups[grp_i]
                del list_of_groups[grp_i]
            list_of_groups.append(new_grp)
            added_pt_to_some_group = True
        if DEBUG:
            print "ritorno la seguente lista di gruppi", list_of_groups
        return list_of_groups, added_pt_to_some_group

    def DBSCAN(self):
        """Execute the DBSCAN algorithm (see Wikipedia for more information,
        but don't look at the pseudocode, do you a favor).

        This method will populate the two lists self.clusters_list and
        self.outliers_list by using them as lists of lists, or namely as list
        of points.

        The method works by iterating on all the points in the dataset,
        checking each one of them on whether it belongs to one of the clusters,
        or to one of the outliers (in this case, a check has to be done in
        order to test if a new group of outliners has the right dimension to be
        promoted to a cluster), or if it's the link between a current existing
        cluster and an outlier, and eventually, if the pt is neither part of an
        existing cluster nor of an outliners group, add it as a new outliner.
        """

        # print self.distance_neighbors
        # raise SystemExit

        for pt in xrange(self.dataset_length):
            if DEBUG:
                print pt
                print "clusters_list before", self.clusters_list
                print "outliers_list before", self.outliers_list

            self.clusters_list, added_cls = self.fit_point_in_groups(
                pt,
                self.clusters_list)
            # if added_cls:
            #     if DEBUG:
            #         print "clusters_list after", self.clusters_list
            #         print "outliers_list after", self.outliers_list
            #     continue

            self.outliers_list, added_out = self.fit_point_in_groups(
                pt, self.outliers_list)

            if added_cls and added_out:
                # Merge the two lists into the last cluster and clean it
                # up with a conversion from list to set to list.
                # print "Adding pt", pt, "to cluster"
                # print "SONO QUI", self.clusters_list[-1],
                # self.outliers_list[-1]
                self.clusters_list[-1].extend(self.outliers_list[-1])
                self.clusters_list[-1] = list(set(self.clusters_list[-1]))
                del self.outliers_list[-1]

            if added_out:

                # This means the point is either part of an already existing
                # cluster and of an outlier group, and both them are the last
                # objects inside clusters and outliers lists.
                # Need to promote the last outlier group to become a part of
                # the last cluster created.

                # Check on every group of outliers if there are enough points
                # to create a new cluster.
                outliers_index_to_become_cluster = []
                for out_grp_i, out_grp in enumerate(self.outliers_list):
                    if len(out_grp) >= self.min_pt_cluster:
                        if DEBUG:
                            print "Grp {} is large enough to become a " \
                                "cluster".format(out_grp)
                        # Retrieve the points from the indexes.
                        outliers_index_to_become_cluster.append(out_grp_i)

                for out_index in outliers_index_to_become_cluster:
                    self.clusters_list.append(
                        self.outliers_list.pop(out_index))
            else:
                if not added_cls:
                    self.outliers_list.append((pt))

            if DEBUG:
                print "clusters_list after", self.clusters_list
                print "outliers_list after", self.outliers_list

        # If the DBSCAN has been executed, let use the print.
        self.dbscan_executed = True

    def __str__(self):
        if not self.dbscan_executed:
            return ("Execute the method DBSCAN on the object, in order to see "
                    "various stats.")
        num_points = 0
        for cl in self.clusters_list:
            num_points += len(cl)
        for out in self.outliers_list:
            num_points += len(out)
        return ("The dataset with length {} has:\n  {} clusters\n+ {} groups "
                "of points not in a cluster (noise)\n= {} total number of "
                "groups.\nThere are {} points at the beginning, and at the end"
                " there are {} points subdivided in clusters and noise".format(
                    self.dataset_length,
                    len(self.clusters_list),
                    1,
                    len(self.clusters_list) + 1,
                    self.dataset_length,
                    num_points
                )
                )

    def n_points_in_largest_cluster(self):
        largest = 0
        for cl in self.clusters_list:
            largest = len(cl)
        return "There are {} points in the largest cluster found.".format(
            str(largest))


if __name__ == "__main__":
    eps = {10: 0.4,
           100: 0.3,
           1000: 0.15,
           10000: 0.15,
           100000: 0.15
           }

    dim = 100
    testfile = "test_files/data_{0}points_{0}dims.dat".format(dim)
    dataset = load_and_parse_matrix(testfile)
    print testfile, eps[dim]
    d = DBSCAN(dataset, eps[dim], 2)

    d.DBSCAN()
    print d.clusters_list
    print d.outliers_list
    print "*" * 20, "\n", d
    print d.n_points_in_largest_cluster(), "\n"

"""
$ time ./4_c.py # 1000
The dataset with length 1000 has:
  9 clusters
+ 1 groups of points not in a cluster (noise)
= 10 total number of groups.
There are 1000 points at the beginning, and at the end there are 1000 points \
                                            subdivided in clusters and noise
There are 272 points in the largest cluster found.


real	1m30.364s
user	1m30.234s
sys	0m0.035s
$ time ./4_c.py # 10000
********************
The dataset with length 10000 has:
  393 clusters
+ 1 groups of points not in a cluster (noise)
= 394 total number of groups.
There are 10000 points at the beginning, and at the end there are 10000 points\
                                            subdivided in clusters and noise
There are 2781 points in the largest cluster found. # don't think it's correct


real	159m40.136s
user	159m19.103s
sys	0m9.042s
$

# FRANZ VERSION
# 100 pts -> 24 pt bigg cluster
# 1000 pts -> 289   ''
# 10000 pts -> 2847  ''

# Jonathan VERSION
1000 pts -> 287 pts in biggest cluster
"""

# for power in xrange(1, 6):
#     print "File: {0}. Epsilon: {1}.".format(10**power, eps[10**power])
#     testfile = "test_files/data_{0}points_{0}dims.dat".format(10**power)
#     dataset = load_and_parse_matrix(testfile)
#
#     d = DBSCAN(dataset, eps[10**power], 2)
#
#     d.DBSCAN()
#     print d
#     print d.n_points_in_largest_cluster(), "\n"

""" list_of_index_of_outs_to_join = []
    for out_index, out_list in enumerate(self.outliers_list):
        if self.is_pt_near_group(pt, out_list):
            out_list.append(pt)
            list_of_index_of_outs_to_join.append(out_index)

            # could a pt be in multiple out_lst? need a set to remove dups?
            joined_index_lists = []
            for list_index_to_join in list_of_index_of_outs_to_join:
                joined_index_lists.extend(out_list[list_index_to_join])"""

# new_cluster = [out_list[pt_i] for pt_i in joined_index_lists]
# # Remove the index from the outliers list.
# out_list = list(set(out_list) - set(joined_index_lists))
# self.clusters_list.append(new_cluster)


"""
# for pti1 in xrange(d.dataset_length):
#     for pti2 in xrange(d.dataset_length):
#         print "dist pt", pti1, "dal punto", pti2, d.jdist(dataset1[pti1],
#                                                           dataset1[pti2])
# for pt1, pti1 in zip(dataset1, xrange(dataset1.get_shape()[0])):
#     for pt2, pti2 in zip(dataset1, xrange(dataset1.get_shape()[0])):
# print "distanza pt", pti1, "dal punto", pti2, jdist(dataset1[pti1],
# dataset1[pti2])


[bebo@alb week4]$ ./4_c.py

point n. 0
clusters_list before []
outliers_list before []
ritorno la seguente lista di gruppi []
ritorno la seguente lista di gruppi []
clusters_list after []
outliers_list after [[0]]

point n. 1
clusters_list before []
outliers_list before [[0]]
ritorno la seguente lista di gruppi []
il punto con ind  1  e' vicino al gruppo con indice  0 ossia  [0]
cancello il gruppo con indice  0  ossia  [0]
ritorno la seguente lista di gruppi [[0, 1]]
clusters_list after [[0, 1]]
outliers_list after []

point n. 2
clusters_list before [[0, 1]]
outliers_list before []
ritorno la seguente lista di gruppi [[0, 1]]
ritorno la seguente lista di gruppi []
clusters_list after [[0, 1]]
outliers_list after [[2]]

point n. 3
clusters_list before [[0, 1]]
outliers_list before [[2]]
il punto con ind  3  e' vicino al gruppo con indice  0 ossia  [0, 1]
cancello il gruppo con indice  0  ossia  [0, 1]
ritorno la seguente lista di gruppi [[0, 1, 3]]

point n. 4
clusters_list before [[0, 1, 3]]
outliers_list before [[2]]
ritorno la seguente lista di gruppi [[0, 1, 3]]
ritorno la seguente lista di gruppi [[2]]
clusters_list after [[0, 1, 3]]
# *** significa che non e' parte di nessun cl o out
outliers_list after [[2], [4]]

point n. 5
clusters_list before [[0, 1, 3]]
outliers_list before [[2], [4]]
ritorno la seguente lista di gruppi [[0, 1, 3]]
#
#
# *** 5 e' vicino a 2
il punto con ind 5 e' vicino al gruppo con indice 0 ossia [2]
cancello il gruppo con indice  0  ossia  [2]
# *** questo e' il problema. include anche 4 quando unisce i due gruppi
ritorno la seguente lista di gruppi [[4], [2, 4, 5]]
clusters_list after [[0, 1, 3], [2, 4, 5]]
outliers_list after [[4]]

point n. 6
clusters_list before [[0, 1, 3], [2, 4, 5]]
outliers_list before [[4]]
il punto con ind  6  e' vicino al gruppo con indice  1 ossia  [2, 4, 5]
cancello il gruppo con indice  1  ossia  [2, 4, 5]
ritorno la seg lista di gruppi [[0, 1, 3], [0, 1, 2, 3, 4, 5, 6]]

point n. 7
clusters_list before [[0, 1, 3], [0, 1, 2, 3, 4, 5, 6]]
outliers_list before [[4]]
ritorno la seg lista di gruppi [[0, 1, 3], [0, 1, 2, 3, 4, 5, 6]]
ritorno la seguente lista di gruppi [[4]]
clusters_list after [[0, 1, 3], [0, 1, 2, 3, 4, 5, 6]]
outliers_list after [[4], [7]]

point n. 8
clusters_list before [[0, 1, 3], [0, 1, 2, 3, 4, 5, 6]]
outliers_list before [[4], [7]]
il punto con ind 8 e' vicino al gruppo con indice 1 ossia [0, 1, 2, 3, 4, 5, 6]
cancello il gruppo con indice  1  ossia  [0, 1, 2, 3, 4, 5, 6]
ritorno la seguente lista di gruppi [[0, 1, 3], [0, 1, 2, 3, 4, 5, 6, 8]]

point n. 9
clusters_list before [[0, 1, 3], [0, 1, 2, 3, 4, 5, 6, 8]]
outliers_list before [[4], [7]]
il punto con ind  9  e' vicino al gruppo con indice  0 ossia  [0, 1, 3]
il punto con ind  9  e' vicino al gruppo con indice  1 ossia  [0,1,2,3,4,5,6,8]
cancello il gruppo con indice  0  ossia  [0, 1, 3]
cancello il gruppo con indice  1  ossia
Traceback (most recent call last):
  File "./4_c.py", line 198, in <module>
    dbs.DBSCAN()
  File "./4_c.py", line 154, in DBSCAN
    pt, self.clusters_list)
  File "./4_c.py", line 125, in fit_point_in_groups
    grp_i, " ossia ", list_of_groups[grp_i]
IndexError: list index out of range
###############################################################################
[bebo@alb week4]$ ./4_c.py
distanza pt 0 dal punto 0
Traceback (most recent call last):
  File "./4_c.py", line 204, in <module>
    print "distanza pt", pti1, "dal punto", pti2, d.jdist(dataset1[pti1],
NameError: name 'd' is not defined
[bebo@alb week4]$ ./4_c.py
distanza pt 0 dal punto 0 0.0
distanza pt 0 dal punto 1 0.0
distanza pt 0 dal punto 2 0.666666666667
distanza pt 0 dal punto 3 0.0
distanza pt 0 dal punto 4 0.5
distanza pt 0 dal punto 5 0.75
distanza pt 0 dal punto 6 0.875
distanza pt 0 dal punto 7 1.0
distanza pt 0 dal punto 8 0.714285714286
distanza pt 0 dal punto 9 0.0
distanza pt 1 dal punto 0 0.0
distanza pt 1 dal punto 1 0.0
distanza pt 1 dal punto 2 0.666666666667
distanza pt 1 dal punto 3 0.0
distanza pt 1 dal punto 4 0.5
distanza pt 1 dal punto 5 0.75
distanza pt 1 dal punto 6 0.875
distanza pt 1 dal punto 7 1.0
distanza pt 1 dal punto 8 0.714285714286
distanza pt 1 dal punto 9 0.0
distanza pt 2 dal punto 0 0.666666666667
distanza pt 2 dal punto 1 0.666666666667
distanza pt 2 dal punto 2 0.0
distanza pt 2 dal punto 3 0.666666666667
distanza pt 2 dal punto 4 1.0
distanza pt 2 dal punto 5 0.4
distanza pt 2 dal punto 6 0.6
distanza pt 2 dal punto 7 1.0
distanza pt 2 dal punto 8 1.0
distanza pt 2 dal punto 9 0.666666666667
distanza pt 3 dal punto 0 0.0
distanza pt 3 dal punto 1 0.0
distanza pt 3 dal punto 2 0.666666666667
distanza pt 3 dal punto 3 0.0
distanza pt 3 dal punto 4 0.5
distanza pt 3 dal punto 5 0.75
distanza pt 3 dal punto 6 0.875
distanza pt 3 dal punto 7 1.0
distanza pt 3 dal punto 8 0.714285714286
distanza pt 3 dal punto 9 0.0
distanza pt 4 dal punto 0 0.5
distanza pt 4 dal punto 1 0.5
distanza pt 4 dal punto 2 1.0
distanza pt 4 dal punto 3 0.5
distanza pt 4 dal punto 4 0.0
distanza pt 4 dal punto 5 1.0
distanza pt 4 dal punto 6 1.0
distanza pt 4 dal punto 7 0.8
distanza pt 4 dal punto 8 0.4
distanza pt 4 dal punto 9 0.5
distanza pt 5 dal punto 0 0.75
distanza pt 5 dal punto 1 0.75
distanza pt 5 dal punto 2 0.4
distanza pt 5 dal punto 3 0.75
distanza pt 5 dal punto 4 1.0
distanza pt 5 dal punto 5 0.0
distanza pt 5 dal punto 6 0.2
distanza pt 5 dal punto 7 0.833333333333
distanza pt 5 dal punto 8 0.875
distanza pt 5 dal punto 9 0.75
distanza pt 6 dal punto 0 0.875
distanza pt 6 dal punto 1 0.875
distanza pt 6 dal punto 2 0.6
distanza pt 6 dal punto 3 0.875
distanza pt 6 dal punto 4 1.0
distanza pt 6 dal punto 5 0.2
distanza pt 6 dal punto 6 0.0
distanza pt 6 dal punto 7 0.8
distanza pt 6 dal punto 8 0.857142857143
distanza pt 6 dal punto 9 0.875
distanza pt 7 dal punto 0 1.0
distanza pt 7 dal punto 1 1.0
distanza pt 7 dal punto 2 1.0
distanza pt 7 dal punto 3 1.0
distanza pt 7 dal punto 4 0.8
distanza pt 7 dal punto 5 0.833333333333
distanza pt 7 dal punto 6 0.8
distanza pt 7 dal punto 7 0.0
distanza pt 7 dal punto 8 0.5
distanza pt 7 dal punto 9 1.0
distanza pt 8 dal punto 0 0.714285714286
distanza pt 8 dal punto 1 0.714285714286
distanza pt 8 dal punto 2 1.0
distanza pt 8 dal punto 3 0.714285714286
distanza pt 8 dal punto 4 0.4
distanza pt 8 dal punto 5 0.875
distanza pt 8 dal punto 6 0.857142857143
distanza pt 8 dal punto 7 0.5
distanza pt 8 dal punto 8 0.0
distanza pt 8 dal punto 9 0.714285714286
distanza pt 9 dal punto 0 0.0
distanza pt 9 dal punto 1 0.0
distanza pt 9 dal punto 2 0.666666666667
distanza pt 9 dal punto 3 0.0
distanza pt 9 dal punto 4 0.5
distanza pt 9 dal punto 5 0.75
distanza pt 9 dal punto 6 0.875
distanza pt 9 dal punto 7 1.0
distanza pt 9 dal punto 8 0.714285714286
distanza pt 9 dal punto 9 0.0
[bebo@alb week4]$ time ./4_c.py
distanza pt 0 dal punto 0 0.0
distanza pt 0 dal punto 1 0.0
distanza pt 0 dal punto 2 0.666666666667
distanza pt 0 dal punto 3 0.0
distanza pt 0 dal punto 4 0.5
distanza pt 0 dal punto 5 0.75
distanza pt 0 dal punto 6 0.875
distanza pt 0 dal punto 7 1.0
distanza pt 0 dal punto 8 0.714285714286
distanza pt 0 dal punto 9 0.0
distanza pt 1 dal punto 0 0.0
distanza pt 1 dal punto 1 0.0
distanza pt 1 dal punto 2 0.666666666667
distanza pt 1 dal punto 3 0.0
distanza pt 1 dal punto 4 0.5
distanza pt 1 dal punto 5 0.75
distanza pt 1 dal punto 6 0.875
distanza pt 1 dal punto 7 1.0
distanza pt 1 dal punto 8 0.714285714286
distanza pt 1 dal punto 9 0.0
distanza pt 2 dal punto 0 0.666666666667
distanza pt 2 dal punto 1 0.666666666667
distanza pt 2 dal punto 2 0.0
distanza pt 2 dal punto 3 0.666666666667
distanza pt 2 dal punto 4 1.0
distanza pt 2 dal punto 5 0.4
distanza pt 2 dal punto 6 0.6
distanza pt 2 dal punto 7 1.0
distanza pt 2 dal punto 8 1.0
distanza pt 2 dal punto 9 0.666666666667
distanza pt 3 dal punto 0 0.0
distanza pt 3 dal punto 1 0.0
distanza pt 3 dal punto 2 0.666666666667
distanza pt 3 dal punto 3 0.0
distanza pt 3 dal punto 4 0.5
distanza pt 3 dal punto 5 0.75
distanza pt 3 dal punto 6 0.875
distanza pt 3 dal punto 7 1.0
distanza pt 3 dal punto 8 0.714285714286
distanza pt 3 dal punto 9 0.0
distanza pt 4 dal punto 0 0.5
distanza pt 4 dal punto 1 0.5
distanza pt 4 dal punto 2 1.0
distanza pt 4 dal punto 3 0.5
distanza pt 4 dal punto 4 0.0
distanza pt 4 dal punto 5 1.0
distanza pt 4 dal punto 6 1.0
distanza pt 4 dal punto 7 0.8
distanza pt 4 dal punto 8 0.4
distanza pt 4 dal punto 9 0.5
distanza pt 5 dal punto 0 0.75
distanza pt 5 dal punto 1 0.75
distanza pt 5 dal punto 2 0.4
distanza pt 5 dal punto 3 0.75
distanza pt 5 dal punto 4 1.0
distanza pt 5 dal punto 5 0.0
distanza pt 5 dal punto 6 0.2
distanza pt 5 dal punto 7 0.833333333333
distanza pt 5 dal punto 8 0.875
distanza pt 5 dal punto 9 0.75
distanza pt 6 dal punto 0 0.875
distanza pt 6 dal punto 1 0.875
distanza pt 6 dal punto 2 0.6
distanza pt 6 dal punto 3 0.875
distanza pt 6 dal punto 4 1.0
distanza pt 6 dal punto 5 0.2
distanza pt 6 dal punto 6 0.0
distanza pt 6 dal punto 7 0.8
distanza pt 6 dal punto 8 0.857142857143
distanza pt 6 dal punto 9 0.875
distanza pt 7 dal punto 0 1.0
distanza pt 7 dal punto 1 1.0
distanza pt 7 dal punto 2 1.0
distanza pt 7 dal punto 3 1.0
distanza pt 7 dal punto 4 0.8
distanza pt 7 dal punto 5 0.833333333333
distanza pt 7 dal punto 6 0.8
distanza pt 7 dal punto 7 0.0
distanza pt 7 dal punto 8 0.5
distanza pt 7 dal punto 9 1.0
distanza pt 8 dal punto 0 0.714285714286
distanza pt 8 dal punto 1 0.714285714286
distanza pt 8 dal punto 2 1.0
distanza pt 8 dal punto 3 0.714285714286
distanza pt 8 dal punto 4 0.4
distanza pt 8 dal punto 5 0.875
distanza pt 8 dal punto 6 0.857142857143
distanza pt 8 dal punto 7 0.5
distanza pt 8 dal punto 8 0.0
distanza pt 8 dal punto 9 0.714285714286
distanza pt 9 dal punto 0 0.0
distanza pt 9 dal punto 1 0.0
distanza pt 9 dal punto 2 0.666666666667
distanza pt 9 dal punto 3 0.0
distanza pt 9 dal punto 4 0.5
distanza pt 9 dal punto 5 0.75
distanza pt 9 dal punto 6 0.875
distanza pt 9 dal punto 7 1.0
distanza pt 9 dal punto 8 0.714285714286
distanza pt 9 dal punto 9 0.0

real	0m0.151s
user	0m0.119s
sys	0m0.027s
[bebo@alb week4]$
"""
