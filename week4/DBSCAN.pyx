#!/usr/bin/env python2

# Imports and definitions needed for the Jaccard distance.
from __future__ import division
import cython
import numpy as np
cimport cython
cimport numpy as np
ctypedef int DTYPE_t

# Imports for the DBSCAN algorithm.
# import cPickle as pickle


def jdist(np.ndarray[DTYPE_t, ndim=1] vec1, np.ndarray[DTYPE_t, ndim=1] vec2):
    """The function accepts the indices of two points (two numpy.arrays like
    [3, 4, 13, 48]) and calculates the Jaccard distance between them.
    """

    cdef int i = 0
    cdef int j = 0

    cdef int intersection = 0

    # In order to calculate the intersection, the iteration stops when one of
    # the two arrays has no more indexes to iterate on.
    while i < vec1.shape[0] and j < vec2.shape[0]:
        if vec1[i] == vec2[j]:
            intersection += 1
            i += 1
            j += 1
        else:
            if vec1[i] > vec2[j]:
                j += 1
            else:
                i += 1

    # The union can be thought like in Venn's diagrams, to avoid calculations.
    cdef int union = vec1.shape[0] + vec2.shape[0] - intersection

    # I imported the true division from python3, so there's no more need of the
    # floating cast (more efficient).
    return 1 - intersection / union


def are_points_close(pt1, pt2, float eps):
    # pt1 and pt2 should be two csr_matrix objects, and we pass only their
    # indices (which are numpy arrays) to the jdist function.
    return jdist(pt1.indices, pt2.indices) <= eps


def is_point_near_group(pt, list_of_pts, eps):
    # Returns true whether the first neighbor point is discovered.
    for p in list_of_pts:
        if are_points_close(pt, p, eps):
            return True
    return False


def point_neighbors(pt, list_of_lists, eps):
    # Return the indices of the groups to which the point is a neighbor.
    return [index for (index, group) in enumerate(list_of_lists)
            if is_point_near_group(pt, group, eps)]


def merge_groups(list_of_groups, indices):
    # Merge together the groups with the indices given and return the index of
    # the new group.

    new_grp = []
    for grpi, grp in enumerate(list_of_groups):
        if grpi in indices:
            new_grp.extend(grp)

    # Remove the groups just merged (in the reverse order to prevent removing
    # wrong elements).
    for i in indices[::-1]:
        del list_of_groups[i]

    # Append the new group and return his index.
    list_of_groups.append(new_grp)
    return len(list_of_groups) - 1


def DBSCAN(dataset, eps, min_pt_cluster):
    clusters = []
    outliers = []

    for pt in dataset:
        # Get the indices of clusters to which the point is close enough.
        clusters_indices = point_neighbors(pt, clusters, eps)

        # Get the indices of outliers to which the point is close enough.
        outliers_indices = point_neighbors(pt, outliers, eps)

        if clusters_indices:
            # Merge the clusters found (the point will be added later).
            index_of_new_cluster = merge_groups(clusters, clusters_indices)

        if outliers_indices:
            # Merge the outliers groups found (the point will be added later).
            index_of_new_outlier = merge_groups(outliers, outliers_indices)

        if clusters_indices and outliers_indices:
            # This means that the point is near to 1+ clusters and to 1+
            # outliers. Merge the new outliers just created above with the new
            # cluster.
            clusters[index_of_new_cluster] += outliers[index_of_new_outlier]

            # Eventually add the point, and then remove the new outlier group.
            clusters[index_of_new_cluster].append(pt)
            del outliers[index_of_new_outlier]

        elif clusters_indices:
            # This means that the point is only part of clusters. Add it to the
            # new cluster just created above.
            clusters[index_of_new_cluster].append(pt)

        elif outliers_indices:
            # This means that the point is only part of outliers. Add it to the
            # new outliers group just created above.
            outliers[index_of_new_outlier].append(pt)

            # And now if the outlier group with the new pt is big enough, let's
            # promote it to become a new cluster.
            if len(outliers[index_of_new_outlier]) >= min_pt_cluster:
                clusters.append(outliers[index_of_new_outlier])

                # And delete the group from the outliers.
                del outliers[index_of_new_outlier]

        else:
            # This means that the point is not part of any cluster nor outlier.
            outliers.append([pt])

    return clusters, outliers
