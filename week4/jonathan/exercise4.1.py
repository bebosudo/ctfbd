#!/usr/bin/env python2

from __future__ import division
from scipy.sparse import csr_matrix
import numpy as np
import time

try:
    import cPickle as pickle
except:
    import pickle

# D : dataset of points
# eps : epsilon
# minPts : minimum number of points to create a cluster


def dbscan(dataset, eps, min_pts):
    clusters = []
    visited_nodes = set()
    noise_nodes = set()
    clustered_points = set()
    points_dist = region_query(dataset, eps)

    for i in xrange(dataset.shape[1]):
        # Create a list of visited nodes
        if i not in visited_nodes:
            visited_nodes.add(i)

            neighbor_points = points_dist[i]

            if len(neighbor_points) < min_pts:
                noise_nodes.add(i)
                clustered_points.add(i)
            else:
                cluster = set()
                expand_cluster(visited_nodes, clustered_points,
                               points_dist, neighbor_points, cluster, min_pts)
                clusters.append(cluster)

    return clusters, noise_nodes


def expand_cluster(visited_points, clustered_points, points_dist, neighbor_points, cluster, minPts):
    while len(neighbor_points) > 0:
        point = neighbor_points.pop()
        if point not in visited_points:
            visited_points.add(point)
            neighbor_points_2 = points_dist[point]

            if len(neighbor_points_2) >= minPts:
                neighbor_points = neighbor_points.union(neighbor_points_2)

        if point not in clustered_points:
            cluster.add(point)
            clustered_points.add(point)


def region_query(dataset, eps):
    neighbors_dist = {}
    for i in xrange(dataset.shape[0]):
        neighbors = set()
        for j in xrange(dataset.shape[1]):
            center_point_indices = dataset[i].indices
            points_indices = dataset[j].indices

            union_len = len(np.union1d(center_point_indices, points_indices))
            # if union_len == 0:
            #    dist = 1
            # else:
            dist = 1 - len(np.intersect1d(center_point_indices,
                                          points_indices)) / union_len
            # print dist
            if dist <= eps:
                neighbors.add(j)
        neighbors_dist[i] = neighbors
    return neighbors_dist


def get_nb_points_largest_cluster(clusters):
    largest = -1
    for cluster in clusters:
        nb_points_current_cluster = len(cluster)
        if nb_points_current_cluster >= largest:
            largest = nb_points_current_cluster

    return largest


def load_matrix(file_name):
    with open(file_name, 'r') as file:
        data = pickle.load(file)
        X = csr_matrix(data)  # coo_matrix(data)
    return X

if "__main__" == __name__:
    t0 = time.time()
    X = load_matrix('./test_files/data_1000points_1000dims.dat')
    # X = load_matrix('./test_files/data_100points_100dims.dat')
    #X = load_matrix('./test_files/data_1000points_1000dims.dat')
    #X = load_matrix('./test_files/data_10000points_10000dims.dat')
    #X = load_matrix('./test_files/data_100000points_100000dims.dat')

    # print X; print X.nnz; print X.todense()

    # print X.data[X.indptr[2]+X.indices[X.indptr[3]:X.indptr[3+1]]]

    clusters, noise_cluster = dbscan(X, 0.15, 2)
    print clusters
    print noise_cluster
    clusters.append(noise_cluster)
    print len(clusters)

    print get_nb_points_largest_cluster(clusters)
    t1 = time.time()
    total = t1 - t0
    print "Time : %d" % total

    # for row_idx in xrange(X.shape[0]):
    #     for col_idx in X[row_idx].indices:
    #         print X[row_idx, col_idx]

    # for i,j,v in zip(X.row, X.col, X.data):
    #    print "(%d, %d), %s" % (i,j,v)
    #X[i,j] = {'data': v, 'type': None}
