#!/usr/bin/env python2

from scipy.sparse import csr_matrix
import numpy as np
from copy import deepcopy
# from jdist import jdist

import cPickle as pickle
from jdist_old import jdist as jdist1
from jdist2 import jdist as jdist2
from jdist3opt import jdist3
from jdist4 import jdist4
# import jdist4

from timeit import timeit
# from ex4_final import DBSCAN

with open("test_files/data_{0}points_{0}dims.dat".format(100)) as fo:
    m = pickle.load(fo)


def test_distance_identity():
    bits = '00000000000000000000000000000000000000000000000000000000000000000'\
        '00000000000000000000000000000000000000000000000000000000000000000'\
        '00000000000000000000000000000000000000000000000000000000000010000'\
        '00000000000000000100000000000000000000000000000000000000000000000'\
        '00000000000000000000000000000000000000000000000000000000000000000'\
        '00000000000000000000000000000000000000000000000000000000000000000'\
        '00000000000000000000000000000000000000000000000000000000000000000'\
        '00000000000000000000000000000000000000000000000000000000000000000'\
        '00000000000000000010000000000000000000000000000000000000000000000'\
        '00000000000000000000000000000000000000000000000000000000000000000'\
        '00000000000000000000000000000000000000000000000000000000000000000'\
        '00000000100000000000000000000000000000000000000000000000000000000'\
        '00000000000000000000000000000000000000000000000000000000000000000'\
        '00000000000000000000000000000000000000000000000000000000000000000'\
        '00000000000000000000000000000000001000000000000000000000000000000'\
        '1000000000000000000000000'
    point1 = csr_matrix(np.array([int(i) for i in bits]))
    point2 = deepcopy(point1)
    assert 0 == jdist3(point1.indices, point2.indices)


def test_distance_opposites():
    negative = '0' * 1000
    positive = '1' * 1000
    point1 = csr_matrix(np.array([int(i) for i in negative]))
    point2 = csr_matrix(np.array([int(i) for i in positive]))
    # Since the intersection is null, the jaccard index is 0, --> jdist == 1
    assert 1 == jdist3(point1.indices, point2.indices)


def test_jdist1():
    # with open("test_files/data_{0}points_{0}dims.dat".format(100)) as fo:
    #     m = pickle.load(fo)
    for p1 in m:
        for p2 in m:
            jdist1(p1.indices, p2.indices)


def test_jdist2():
    # with open("test_files/data_{0}points_{0}dims.dat".format(100)) as fo:
    #     m = pickle.load(fo)
    for p1 in m:
        for p2 in m:
            jdist2(p1.toarray()[0], p2.toarray()[0])


def test_jdist3():
    # with open("test_files/data_{0}points_{0}dims.dat".format(100)) as fo:
    #     m = pickle.load(fo)
    for p1 in m:
        for p2 in m:
            jdist3(p1.indices, p2.indices)


def test_jdist4():
    for p1 in m:
        for p2 in m:
            jdist4(p1.indices, p2.indices)


def test_empty_for_cycles():
    for p1 in m:
        for p2 in m:
            pass


def performances_of_jdist(number_executions=10):
    f = timeit("test_empty_for_cycles()",
               setup="from __main__ import test_empty_for_cycles",
               number=number_executions)
    print "time needed to execute two empty for cycles:", f

    # print "old jdist function"
    # print timeit("test_jdist1()", setup="from __main__ import test_jdist1",
    #              number=10)
    # print "optimized jdist function"
    # print timeit("test_jdist2()", setup="from __main__ import test_jdist2",
    #              number=10)
    j3 = timeit("test_jdist3()", setup="from __main__ import test_jdist3",
                number=number_executions)
    print "optimized jdist function 3:", j3, ". Without the for loops:", j3 - f

    j4 = timeit("test_jdist4()", setup="from __main__ import test_jdist4",
                number=number_executions)
    print "jdist function 4:", j4, ". Without the for loops:", j4 - f
    # timeit("[[jdist2(p1,p2) for p2 in m] for p1 in m]", number=1000)


def test_merger_function():
    def merger_function(indices, list_of_groups):
        if len(indices) > 1:
            # Merge all the sets in the first.
            for i in indices[1:]:
                # if DEBUG:
                #     print "faccio il merge di", list_of_groups[i], "in", \
                #         list_of_groups[0]
                list_of_groups[0].update(list_of_groups[i])

            # Delete the sets starting from the last one to the right until
            # reaching the second to the left.
            for i in indices[:0:-1]:
                del list_of_groups[i]

    l = [{3, 6, 7, 8}, {5, 6, 8, 14}, {0, 9, 8, 14, 7, 2}, {4, 5, 9, 2, 10}]
    indices = [0, 1]
    merger_function(indices, l)
    assert [set([3, 5, 6, 7, 8, 14]), set([0, 2, 7, 8, 9, 14])] == l


if __name__ == "__main__":
    # test_distance_identity()
    # test_distance_opposites()
    performances_of_jdist()
    # test_merger_function()

    print "All tests executed correctly."
