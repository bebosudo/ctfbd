#!/usr/bin/env python2

from DBSCAN import DBSCAN
import cPickle as pickle

# In order to read arguments passed to the script.
import sys

if __name__ == '__main__':
    # Almost all of the following code is used only to check input.

    eps = {10: 0.4,
           100: 0.3,
           1000: 0.15,
           10000: 0.15,
           100000: 0.15
           }

    if len(sys.argv) == 1:
        print "Usage: insert the number of dimensions to which test the " \
            "script on.\nCurrently dimensions available:", sorted(eps.keys())
        raise SystemExit
    elif len(sys.argv) != 2:
        print "Only an integer argument is allowed."
        raise SystemExit

    try:
        dim = int(sys.argv[1])
    except ValueError:
        print "Only an integer argument is allowed."
        raise SystemExit

    testfile = "test_files/data_{0}points_{0}dims.dat".format(dim)

    try:
        with open(testfile) as fo:
            dataset = pickle.load(fo)
    except IOError:
        print "Input file {} not found.".format(testfile)
        raise SystemExit

    minimum_point_per_cluster = 2

    # This is the only important line in this main function.
    clusters, outliers = DBSCAN(
        dataset, eps[dim], minimum_point_per_cluster)

    # Pretty printing functions.
    # Get the number of points in the largest cluster.
    largest = 0
    for cl in clusters:
        if len(cl) > largest:
            largest = len(cl)

    # Get the total number of points in all the clusters and the outliers
    # groups.
    num_points = 0
    for cl in clusters:
        num_points += len(cl)
    for out in outliers:
        num_points += len(out)

    print ("The dataset with length {} has:\n  {} clusters\n+ {} groups "
           "of points not in a cluster (noise)\n= {} total number of "
           "groups.\nThere are {} points at the beginning, and at the end"
           " there are {} points subdivided in clusters and noise\n"
           "There are {} points in the largest cluster found.\n"
           "".format(
               dim,
               len(clusters),
               1,
               len(clusters) + 1,
               dim,
               num_points,
               largest
           )
           )
