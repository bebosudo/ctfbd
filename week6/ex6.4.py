#!/usr/bin/env python2
#

from neo4j.v1 import GraphDatabase, basic_auth

driver = GraphDatabase.driver(
    "bolt://localhost", auth=basic_auth("neo4j", "week6"))

with driver.session() as session:

    customers = session.run("""
        MATCH (p: Product {productID: "7"}) <-[:ORDERS]- (:Order) <-[:PURCHASED]- (c: Customer)
        WITH DISTINCT c
        RETURN c.customerID, c.contactName, c.companyName;
        """)

    # Instead of running another query just to count the number of lines we
    # are going to print, we could instead increment a variable inside the
    # following for cycle every time we print a result.
    # i = 0
    print "customerID\tcontactName\t\tcompanyName"
    for record in customers:
        # i =+ 1
        customerID, contactName, companyName = record[0], record[1], record[2]
        print u"{}\t\t{}\t\t{}".format(customerID, contactName, companyName)

    # The following query is practically the same as the preceding,
    # differentiated only by the counting operation at the end.
    no_customers = session.run("""
        MATCH (p: Product {productID: "7"}) <-[:ORDERS]- (:Order) <-[:PURCHASED]- (c: Customer)
        WITH DISTINCT c
        RETURN count(c);
        """)

    print("""\nThere are {} number of customers who bought "Uncle Bob's"""
          """ Organic Dried Pears" (productID='7').""".format(no_customers.peek()[0]))
