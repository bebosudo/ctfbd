#!/usr/bin/env python2
#

from neo4j.v1 import GraphDatabase, basic_auth

driver = GraphDatabase.driver(
    "bolt://localhost", auth=basic_auth("neo4j", "week6"))

with driver.session() as session:

    result = session.run("""
        MATCH (c :Customer {customerID: "ALFKI"}) -[PURCHASED]-> (ord: Order) -[ORDERS]-> (prod :Product)
        WITH ord, count(prod) as c_prod
        WHERE c_prod > 1
        MATCH (ord) -[ORDERS]-> (p :Product)
        RETURN ord.orderID, ord.orderDate, p.productID, p.productName
        """)

    print "orderID\torderDate\t\tproductID\tproductName"
    for record in result:
        # The order ID will be repeated multiple times, because in the same
        # order there can be multiple products.
        orderID, orderDate, productID, productName = (record[0], record[1],
                                                      record[2], record[3])
        print u"{}\t{}\t{}\t\t{}".format(orderID, orderDate,
                                         productID, productName)
