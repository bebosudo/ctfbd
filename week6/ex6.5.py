#!/usr/bin/env python2
#

from neo4j.v1 import GraphDatabase, basic_auth

driver = GraphDatabase.driver(
    "bolt://localhost", auth=basic_auth("neo4j", "week6"))

with driver.session() as session:

    products = session.run("""
        MATCH (p: Product {productID: '7'}) <-[:ORDERS]- (o :Order) <-[:PURCHASED]- (c: Customer)
        WITH c
        MATCH (c) -[:PURCHASED]-> (:Order) -[:ORDERS]-> (p :Product)
        RETURN DISTINCT p.productID, p.productName;
        """)

    # Write another query to get the number of products.

    # Instead of running another query just to count the number of lines we
    # are going to print, we could instead increment a variable inside the
    # following for cycle every time we print a result.
    # i = 0
    print "productID:\tproductName:"
    for record in products:
        # i =+ 1
        productID, productName = record[0], record[1]
        print u"{}\t\t{}".format(productID, productName)

    no_products = session.run("""
        MATCH (p: Product {productID: '7'}) <-[:ORDERS]- (o :Order) <-[:PURCHASED]- (c: Customer)
        WITH c
        MATCH (c) -[:PURCHASED]-> (:Order) -[:ORDERS]-> (p :Product)
        WITH DISTINCT p
        RETURN count(p);
        """)

    print("\nThere are {} number of different products bought from persons who"
          """ also bought "Uncle Bob's Organic Dried Pears" """
          """(productID='7').""".format(no_products.peek()[0])
          )
